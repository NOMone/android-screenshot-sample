package com.nomone.samples.screenshot;

import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.view.Display;
import android.view.Surface;

import java.nio.ByteBuffer;

/**
* Created by raslanove on 4/10/18.
*/

public class VirtualDisplayImage implements ImageReader.OnImageAvailableListener {

    private VirtualDisplayImageListener listener;

    private final int width;
    private final int height;

    private final ImageReader imageReader;
    private Bitmap latestBitmap=null;

    VirtualDisplayImage(Display displayToClone, VirtualDisplayImageListener listener, Handler handler) {

        this.listener = listener;

        // Create an image reader with the same display dimensions (or at least the same aspect ratio),
        Point size = new Point();
        displayToClone.getSize(size);

        int width  = size.x;
        int height = size.y;

        // No more than 500k pixels,
        while (width*height > (2<<19)) {
            width  = width >>1;
            height = height>>1;
        }

        this.width  =  width;
        this.height = height;

        imageReader = ImageReader.newInstance(width, height, PixelFormat.RGBA_8888, 2);
        imageReader.setOnImageAvailableListener(this, handler);
    }

    @Override
    public void onImageAvailable(ImageReader reader) {

        final Image image = imageReader.acquireLatestImage();

        if (image != null) {

            Image.Plane[] planes = image.getPlanes();

            ByteBuffer buffer = planes[0].getBuffer();

            int pixelStride = planes[0].getPixelStride();        // Advance per pixel.
            int   rowStride = planes[0].  getRowStride();        // Advance per row.
            int  rowPadding = rowStride - pixelStride * width;   // Additional bytes/pixels added to the row after
                                                                 // the actual width pixels.
            int bitmapWidth = width + rowPadding / pixelStride;  // Width including the extra padding pixels.

            // If the bitmap needs to be created/recreated,
            if ((latestBitmap == null) ||
                (latestBitmap.getWidth () != bitmapWidth) ||
                (latestBitmap.getHeight() !=      height)) {

                // Dispose of any existing bitmap,
                if (latestBitmap != null) latestBitmap.recycle();

                latestBitmap = Bitmap.createBitmap(bitmapWidth, height, Bitmap.Config.ARGB_8888);
            }

            // Get the buffer contents,
            latestBitmap.copyPixelsFromBuffer(buffer);

            // Release the image,
            image.close();

            // Notify the listener of the new image,
            listener.onNewFrame(latestBitmap);
        }
    }

    Surface getSurface() {
        return(imageReader.getSurface());
    }

    int getWidth() {
        return(width);
    }

    int getHeight() {
        return(height);
    }

    void close() {
        imageReader.close();
    }

    public interface VirtualDisplayImageListener {
        void onNewFrame(Bitmap bitmap);
    }
}
