package com.nomone.samples.screenshot;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.AudioManager;
import android.media.MediaScannerConnection;
import android.media.ToneGenerator;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.util.Log;
import android.view.WindowManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by raslanove on 4/9/18.
 *
 */

public class ScreenshotService extends Service implements VirtualDisplayImage.VirtualDisplayImageListener {

    // Screen capture intent results,
    static final String EXTRA_RESULT_CODE = "resultCode";
    static final String EXTRA_RESULT_INTENT = "resultIntent";
    private int resultCode;
    private Intent resultData;

    // Notification stuff,
    static final String ACTION_RECORD = BuildConfig.APPLICATION_ID + ".RECORD";
    static final String ACTION_EXIT = BuildConfig.APPLICATION_ID + ".EXIT";
    private static final int NOTIFY_ID = 9906;

    // Media projection and virtual display stuff to take screenshots,
    private MediaProjectionManager mediaProjectionManager;
    private MediaProjection mediaProjection;

    static final int VIRT_DISPLAY_FLAGS = DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY | DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC;
    private VirtualDisplay virtualDisplay;
    private VirtualDisplayImage virtualDisplayImage;

    // Callbacks handler,
    final private HandlerThread callbacksHandlerThread = new HandlerThread(getClass().getSimpleName(), android.os.Process.THREAD_PRIORITY_BACKGROUND);
    private Handler callbacksHandler;

    // To make some noise
    private final ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);

    @Override
    public void onCreate() {
        super.onCreate();

        mediaProjectionManager = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);

        // Start the callbacks handling thread,
        callbacksHandlerThread.start();
        callbacksHandler = new Handler(callbacksHandlerThread.getLooper());
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new IllegalStateException("Binding not supported. Go away.");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent.getAction() == null) {

            // Service started from the MainActivity,
            resultCode = intent.getIntExtra(EXTRA_RESULT_CODE, 1337);
            resultData = intent.getParcelableExtra(EXTRA_RESULT_INTENT);
            foregroundify();

        } else if (intent.getAction().equals(ACTION_RECORD)) {

            // Service started by pressed record in the notification,
            if (resultData != null) {

                // We were granted recording access,
                startCapture();
            } else {

                // Redo from start,
                Intent ui = new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ui);
            }

        } else if (intent.getAction().equals(ACTION_EXIT)) {

            // Service started by pressed exit in the notification,
            toneGenerator.startTone(ToneGenerator.TONE_PROP_NACK);
            stopForeground(true);
            stopSelf();
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        stopCapture();
        super.onDestroy();
    }

    private void stopCapture() {

        if (mediaProjection!=null) {
            mediaProjection.stop();
            virtualDisplay.release();
            virtualDisplayImage.close();
            mediaProjection = null;
        }
    }

    private void startCapture() {

        // Create media projection,
        MediaProjection.Callback mediaProjectionCallBack = new MediaProjection.Callback() {
            @Override public void onStop() {
                virtualDisplay.release();
            }
        };
        mediaProjection = mediaProjectionManager.getMediaProjection(resultCode, resultData);
        mediaProjection.registerCallback(mediaProjectionCallBack, callbacksHandler);

        // Create virtual display,
        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        virtualDisplayImage = new VirtualDisplayImage(windowManager.getDefaultDisplay(), this, callbacksHandler);
        virtualDisplay = mediaProjection.createVirtualDisplay(
                "nomoneScreenShooter",
                virtualDisplayImage.getWidth(), virtualDisplayImage.getHeight(), getResources().getDisplayMetrics().densityDpi,
                VIRT_DISPLAY_FLAGS,
                virtualDisplayImage.getSurface(),
                null, callbacksHandler);
    }

    @Override
    public void onNewFrame(Bitmap bitmap) {

        // Save to a file,
        saveFrame(bitmap);

        // One frame is enough,
        toneGenerator.startTone(ToneGenerator.TONE_PROP_ACK);
        stopCapture();
    }

    private void saveFrame(Bitmap bitmap) {

        // Since the bitmap could be larger than the actual display, crop to the display size,
        Bitmap croppedBitmap = Bitmap.createBitmap(
                bitmap,
                0, 0, virtualDisplayImage.getWidth(), virtualDisplayImage.getHeight());

        // Generate PNG file data,
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        croppedBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        final byte[] pngData = byteArrayOutputStream .toByteArray();

        // Save the data to a file,
        new Thread() {

            @Override public void run() {

                // Use the apps storage folder on the sdcard,
                File file = new File(getExternalFilesDir(null),"screenshot.png");

                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(file);

                    fileOutputStream.write(pngData);
                    fileOutputStream.flush();
                    fileOutputStream.getFD().sync();
                    fileOutputStream.close();

                    // Scan the image to allow it to show in the gallery and stuff (but it never worked!),
                    MediaScannerConnection.scanFile(
                            ScreenshotService.this,
                            new String[] {file.getAbsolutePath()},
                            new String[] {"image/png"},
                            null);
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName(), "Exception writing out screenshot", e);
                }
            }
        }.start();
    }

    private void foregroundify() {

        Notification.Builder notificationBuilder = new Notification.Builder(this);

        notificationBuilder.setAutoCancel(true).setDefaults(Notification.DEFAULT_ALL);
        notificationBuilder
                .setContentTitle(getString(R.string.app_name))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(0xffa00000)
                .setTicker(getString(R.string.app_name));

        notificationBuilder.addAction(R.drawable.ic_record_white_24dp, "Record", buildPendingIntent(ACTION_RECORD));
        notificationBuilder.addAction(R.drawable.ic_exit_white_24dp, "Exit", buildPendingIntent(ACTION_EXIT));

        startForeground(NOTIFY_ID, notificationBuilder.build());
    }

    private PendingIntent buildPendingIntent(String action) {

        // Build an intent that calls this service but with the specified action code,
        Intent intent = new Intent(this, getClass());
        intent.setAction(action);
        return PendingIntent.getService(this, 0, intent, 0);
    }
}